
const CommonUtils = {
    getCurrentDate : async () => {
        var currentDate = new Date();
        var dd = await CommonUtils.formatMonthOrDate("" + currentDate.getDate());
        var mm = await CommonUtils.formatMonthOrDate("" + (currentDate.getMonth() + 1));
        var yyyy = currentDate.getFullYear();
        return dd + "-" + mm + "-" + yyyy;
    },

    formatMonthOrDate : async(monthOrDate) => {
        if(monthOrDate.length == 1)
            return '0' + monthOrDate;
    },
    
    isNullOrBlank: (obj) => {
        return ("null" == obj || null == obj || "undefined" == obj || '' === obj);
    },

    isNotNullOrBlank: (obj) => {
        return (obj && "null" != obj && null != obj && '' != obj);
    },
}

module.exports = CommonUtils;