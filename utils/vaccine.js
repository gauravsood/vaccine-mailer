const request = require('request');
const CommonUtils = require('./commonutils');
const email = require('./email');
require('dotenv').config();


const Vaccine = {
    getVaccineAppointments : async () => {
        var date = await CommonUtils.getCurrentDate();
        console.log('Current Date: ' + date);
        const apiUrl = 'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode=' + process.env.PINCODE + '&date=' + date;
        console.log('API URL: ' + apiUrl);

        const options = {
            url : apiUrl,
            method : 'GET',
            headers : {
                'User-Agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0',
            },
        }

        await request.get(options, function(err, response, body) {
            let apiResponse = JSON.parse(body);
            let centers = apiResponse.centers;
            console.log(centers);
            if(centers.length == 0) {
                console.log('No vaccination centers available');
                let text = 'No vaccination centers available for pincode ' + process.env.PINCODE;
                email.sendMail(text);
            } else {
                console.log('Vaccination centers available.');
                centers.forEach(center => {
                    console.log('Name: ' + center.name);
                    if(CommonUtils.isNotNullOrBlank(center.sessions) && center.sessions.length > 0) {
                        console.log('Sessions available in center ' + center.name);
                        center.sessions.forEach(session => {
                            if(session.min_age_limit == 18 && session.available_capacity > 0) {
                                console.log('Vaccinations available at center for 18+');
                                let text = "Vaccination available at " + center.name + " at " + center.address + " on " + new Date() + ". Available doses: " + session.available_capacity;
                                email.sendMail(text);
                            } else {
                                console.log('No Vaccinations available at center for 18+');
                            }
                        });
                    }
                });

            }
        });
    }
}

module.exports = Vaccine;