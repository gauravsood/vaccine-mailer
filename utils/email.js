const nodemailer = require('nodemailer');
const AWS = require('aws-sdk');
require('dotenv').config();

const Email = {
    sendMail : async (text) => {
        console.log('Sending email to ' + process.env.AUDIENCE_EMAIL);
        
        const sesConfig = {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_ACCESS_KEY_SECRET,
            region: process.env.AWS_SES_REGION,
        }

        var params = {
            Source: process.env.AUDIENCE_EMAIL,
            Destination: {
                ToAddresses: [
                    process.env.AUDIENCE_EMAIL,
                ]
            },
            ReplyToAddresses: [],
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: text,
                    }
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: 'Vaccine Mailer',
                }
            }
        };

        new AWS.SES(sesConfig).sendEmail(params).promise().then((res) => {
            console.log(res);
        });

    }
}

module.exports = Email;