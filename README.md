Basic application which calls the CoWin Public APIs to fetch available appointments in a week in the configured Pin Code and sends an email if an appointment is available. Requires the use of AWS Simple Email Service for sending emails.
# Environment Setup
Following environment variables need to be set:

- `PINCODE`: Pincode to search appointments in (eg: 411045)
- `SCHEDULE_INTERVAL_MS`: Interval in ms to recheck appointments
- `AWS_ACCESS_KEY_ID`: Access Key ID for AWS SES
- `AWS_ACCESS_KEY_SECRET`: Access Key Secret for AWS SES
- `AWS_SES_REGION`: AWS SES Region
- `AUDIENCE_EMAIL`: Email address to send notifications to


## Deployment
- Run `npm install` to install all the dependencies
- Run `node index.js` to start the server.