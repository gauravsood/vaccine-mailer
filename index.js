const vaccine = require('./utils/vaccine');
require('dotenv').config();
console.log('Server started');

console.log('Get appointments');
vaccine.getVaccineAppointments();

console.log('Schedule appointment on configured interval');
setInterval(() => {
    vaccine.getVaccineAppointments();
}, parseInt(process.env.SCHEDULE_INTERVAL_MS));
